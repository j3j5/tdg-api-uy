<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Tile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class GameController extends Controller
{
    protected Game $game;
    protected string $callback;

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request, Game $game)
    {
        $rules = [
            'callback' => 'required|string',
            'action' => 'required|string|in:desc,tiles,log_action',
        ];
        // GET requests redirect on validation exception instead of erroring out
        try {
            $request->validate($rules);
        } catch (ValidationException $e) {
            abort(400, $e->getMessage());
        }

        $this->game = $game;
        $this->callback = $request->input('callback');
        $action = $request->input('action');

        return match ($action) {
            'desc' => $this->description(),
            'tiles' => $this->tiles($request),
            'log_action' => $this->log($request),
            default => $this->callback
                ? response()->jsonp(
                    $this->callback,
                    [
                        'error' => 'Invalid action, action "' . $action . '" is not supported',
                    ]
                )
                : response()->json([
                    'error' => 'Invalid action, action "' . $action . '" is not supported',
                ]),
        };
    }

    protected function description()
    {
        return response()->jsonp($this->callback, $this->game->description);
    }

    protected function tiles(Request $request): JsonResponse
    {
        // Number of games to return
        $num = $request->integer('num', 1);

        $tiles = $this->game->tiles()->pending()->inRandomOrder()->take($num)->get();

        $data['tiles'] = $tiles->map(fn (Tile $tile) => [
            'id' => $tile->id,
            'sections' => $tile->sections,
            'controls' => $tile->controls,
        ])->toArray();

        return response()->jsonp($this->callback, $data);
    }

    protected function log(Request $request)
    {
        $tile = Tile::findOrFail($request->integer('tile'));
        $user = $request->string('user')->toString();
        $decision = $request->string('decision')->toString();

        match($decision) {
            'yes' => $tile->update([
                'username' => $user,
                'status' => Tile::DONE,
            ]),
            'no' => $tile->update([
                'username' => $user,
                'status' => Tile::INVALID,
            ]),
            'skip' => Log::debug('skip request', Arr::wrap($request->input())),
            default => Log::debug($decision . ' request', Arr::wrap($request->input())),
        };

        return response()->json('', Response::HTTP_ACCEPTED);
    }
}
