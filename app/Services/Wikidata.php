<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class Wikidata
{
    private readonly string $baseUrl;

    public function __construct(private readonly string $lang = 'es')
    {
        $this->baseUrl = 'https://www.wikidata.org/w/api.php';
    }

    public function search(string $query) : array
    {
        $params = [
            'origin' => '*',
            'action' => 'wbsearchentities',
            'format' => 'json',
            'limit' => '5',
            'continue' => '0',
            'language' => $this->lang,
            'uselang' => $this->lang,
            'search' => $query,
            'type' => 'item',
        ];

        $response = Http::get($this->baseUrl, $params)->throw();

        return $response->json('search');
    }
}
