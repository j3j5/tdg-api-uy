<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Tile;
use App\Services\Wikidata;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Nomenclator extends Command
{
    private Wikidata $api;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:nomenclator {--skip=0} {--remove-progress-bar}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create game and tiles for Nomenclator';

    /**
     * Execute the console command.
     */
    public function handle(Wikidata $api)
    {
        $this->api = $api;

        $game = Game::updateOrCreate([
            'slug' => 'nomenclator',
        ], [
            'name' => 'Nomenclator de Montevideo',
            'description' => [
                'label' => [
                    'es' => 'Nomenclator de Montevideo',
                    'en' => 'Montevideo street names',
                ],
                'description' => [
                    'es' => 'Encuentra la entidad por la que la calle fue nombrada (modifica P138)',
                    'en' => 'Find the entity for which the street was named after (changes P138)',
                ],
                'icon' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Letras_Montevideo_en_Pocitos.jpg/320px-Letras_Montevideo_en_Pocitos.jpg',
            ],
        ]);

        $filepath = resource_path('data/streets-no-named-after.json');
        $streets = json_decode(json: file_get_contents($filepath), associative: true);

        $nomenclator = $this->readNomenclatorData();
        $classifiedStreets = $this->readClassifiedStreetsData();

        if (!$this->option('remove-progress-bar')) {
            $this->output->progressStart(count($streets));
        }

        $allowedCategories = [
            'Hombre',
            'Mujer',
            'Naturaleza',
            'Geográfico',
            'Instituciones',
            'Arte y Cultura',
        ];

        foreach($streets as $i => $street) {
            if (!$this->option('remove-progress-bar')) {
                $this->output->progressAdvance(1);
            }

            if ($i < $this->option('skip')) {
                continue;
            }

            if (!isset($street['clave_de_vía']) || empty($street['clave_de_vía'])) {
                if ($this->output->isVerbose()) {
                    $this->info('NO hay clave de vía');
                }
                if ($this->output->isVeryVerbose()) {
                    dump($street);
                }
                continue;
            }

            $nomenclatorVia = $nomenclator->firstWhere('VIA', $street['clave_de_vía']);
            $classifiedVia = $classifiedStreets->firstWhere('cod_nombre', $street['clave_de_vía']);
            if (!isset($classifiedVia['CATEGORIA']) || !in_array($classifiedVia['CATEGORIA'], $allowedCategories)) {
                if ($this->output->isDebug()) {
                    $this->info($street['itemsLabel'] . ' not in allowed categories');
                }
                continue;
            }

            $qid = str_replace('http://www.wikidata.org/entity/', '', $street['items']);
            try {
                $tile = [
                    'qid' => $qid,
                    'name' => $street['itemsLabel'],
                    'game_id' => $game->id,
                    'status' => Tile::PENDING,
                    'sections' => [
                        ['type' => 'item', 'q' => $qid],
                        [
                            'type' => 'text',
                            'title' => $street['itemsLabel'],
                            'url' => 'https://montevideo.gub.uy/aplicacion/nomenclator/' . $street['clave_de_vía'],
                        ],
                    ],
                    // 'controls' => $this->getOptionsForStreet($qid, $classifiedVia['nom_calle']),
                    'controls' => $this->getOptionsForStreet($qid, $nomenclatorVia['NOMBRE']),
                ];
                if (isset($nomenclatorVia['SIGNIFICADO_VIA']) && !empty($nomenclatorVia['SIGNIFICADO_VIA'])) {
                    $tile['sections'][1]['text'] = $nomenclatorVia['SIGNIFICADO_VIA'];
                }

                Tile::create($tile);
            } catch (Exception $e) {
                Log::notice($e->getMessage());
            }
        }

        if (!$this->option('remove-progress-bar')) {
            $this->output->progressFinish();
        }
    }

    private function getOptionsForStreet(string $qid, string $name): array
    {
        $toTweak = [
            '  ' => ' ',
            'CORONEL ALEGRE' => 'BUENAVENTURA ALEGRE',
            'GENERAL ACHA' => 'MARIANO ACHA',
            'GRAL. FLORES' => 'VENANCIO FLORES',
            'RAMBLA SUD AMERICA' => 'SUDAMERICA',
        ];
        foreach ($toTweak as $search => $replace) {
            $name = str_replace($search, $replace, $name);
        }

        $toRemove = [
            'RAMBLA COSTANERA',
            'GENERAL DE DIVISION',
            'PASAJE PEATONAL',
            'PASAJE DE ',
            'PROF. DR.',
            'AVENIDA DR.',
            'AVENIDA  DR.',
            'ING. ',
            'GENERAL',
            'ARQ',
            'DR.',
            'GRAL',
            'CONOCIDA',
            'ASEN MANGA 2003',
            'CAMINO',
            'DOCTOR',
            'AVENIDA',
            'PASAJE',
            'CAPITAN',
            'CONTINUACION ',
            'CONT ',
            'CAMINO ',
        ];
        $cleanName = trim(str_replace($toRemove, '', $name));
        usleep(random_int(1, 2000000));

        $options = $this->api->search($cleanName);

        if (empty($options)) {
            throw new Exception('No options for ' . $cleanName . '(' . $qid . ')');
        }

        $controls = [];
        foreach ($options as $option) {
            info('option', $option);
            $description = $option['id'];
            if (isset($option['description'])) {
                $description = $option['description'] . ' - ' . $option['id'];
                // Idea, check P31 (instance of) of the entity to see whether it's a street (Q79007)
                // See https://www.wikidata.org/w/api.php?action=wbgetclaims&property=P31&entity=Q56752889
                foreach(['calle', 'avenida'] as $needle) {
                    if (mb_stripos(needle:  $needle, haystack: $option['description']) !== false) {
                        $this->info(
                            PHP_EOL . 'discarding suggestion:' . PHP_EOL . $option['label']
                            . ' (' . $option['description'] . ')'
                        );
                        continue(2);
                    }
                }
            }

            $id = $option['id'];
            $numericId = str_replace('Q', '', $option['id']);
            $controls[] = [
                'type' => 'green',
                'decision' => 'yes',
                'label' => $option['label'] . ' (' . $description . ')', //etiqueta que aparecerá en el botón
                'api_action' => [
                    'action' => 'wbcreateclaim',
                    'entity' => $qid, // qid from the item to be modified
                    'property' => 'P138', // property to be modified
                    'snaktype' => 'value',
                    'value' => "{\"entity-type\":\"item\",\"id\":\"$id\",\"numeric-id\":$numericId}", // value for the property
                ],
            ];
        }

        if (empty($controls)) {
            throw new Exception('No valid controls were found!');
        }

        return [$controls];
    }

    private function readNomenclatorData(): Collection
    {
        $data = collect();
        $keys = null;
        $filepath = resource_path('data/nomenclator-mvd.csv');
        $handle = fopen($filepath, 'r');
        while ($line = fgetcsv(stream: $handle, separator: ';')) {
            if ($keys === null) {
                $keys = array_map(trim(...), $line);
                continue;
            }
            $data->push(array_combine($keys, array_map(trim(...), $line)));
        }
        fclose($handle);

        return $data;
    }

    private function readClassifiedStreetsData(): Collection
    {
        $data = collect();
        $keys = null;
        $filepath = resource_path('data/calles_marzo2023.csv');
        $handle = fopen($filepath, 'r');
        while ($line = fgetcsv($handle)) {
            if ($keys === null) {
                $keys = array_map(trim(...), $line);
                continue;
            }
            $data->push(array_combine($keys, array_map(trim(...), $line)));
        }
        fclose($handle);

        return $data;
    }
}
