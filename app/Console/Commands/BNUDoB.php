<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Tile;
use App\Traits\HandlesBNUArtists;
use App\Traits\HandlesCSV;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use RuntimeException;

class BNUDoB extends Command
{
    use HandlesCSV, HandlesBNUArtists;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:bnu-dob {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private Game $game;
    private LazyCollection $bnuArtists;
    /** Date of birth */
    private string $property = 'P569';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->game = Game::where('slug', 'bnu-ocupacion')->firstOrFail();

        $offset = (int) $this->option('offset');

        $artistsMissingDoB = $this->getArtistsMissingDoB();
        $this->output->progressStart($artistsMissingDoB->count());

        $this->output->progressAdvance($offset);

        $this->bnuArtists = $this->getArtistsFromBNU();

        $artistsMissingDoB->skip($offset)->each($this->createTile(...));
        $this->output->progressFinish();
    }

    public function createTile(array $artist, int $index) : void
    {
        $bnu = $this->bnuArtists->firstWhere('id', $artist['bnu_id']);
        $this->output->progressAdvance();

        $qid = str_replace('http://www.wikidata.org/entity/', '', $artist['url']);
        try {
            $tile = [
                'qid' => $qid,
                'property' => $this->property,
                'name' => $artist['name'],
                'game_id' => $this->game->id,
                'status' => Tile::PENDING,
                'sections' => [
                    ['type' => 'item', 'q' => $qid],
                    [
                        'type' => 'text',
                        'title' => $artist['name'] . ' - ¿Fecha de nacimiento (' . $this->property . ')?',
                        'text' => $bnu['Datos Biog./Hist.'],
                    ],
                ],
                'controls' => $this->getControls($qid, $bnu),
            ];
            Tile::updateOrCreate(['qid' => $qid, 'property' => $this->property], $tile);
        } catch(RuntimeException) {
            file_put_contents(
                filename: resource_path('data/dob-not-found.txt'),
                data: $artist['name'] . ': ' . Arr::get($bnu, 'Datos Biog./Hist.', '') . PHP_EOL,
                flags: FILE_APPEND
            );
        }

    }

    private function getControls(string $qid, array $bnuArtist) : array
    {
        $date = $bnuArtist['date_born'];
        if (empty($date)) {
            throw new RuntimeException('No date found');
        }

        if (mb_strlen($date) === 4) {
            $carbonDate = Carbon::createFromFormat('Y', $date)->startOfYear();
            $precission = 9; // precisión: year
            $label = $date;
        } elseif (preg_match('/\d{4}-\d{2}-\d{2}/', $date)) {
            $carbonDate = Carbon::createFromFormat('Y-m-d', $date)->startOfday();
            $precission = 11; // precisión: day
            $label = $carbonDate->locale('es_UY')->isoFormat('LL');
        } elseif(preg_match('/\d{4}-\d{2}/', $date)) {
            $carbonDate = Carbon::createFromFormat('Y-m', $date)->startOfMonth();
            $precission = 10; // precisión: month
            $label = $carbonDate->locale('es_UY')->monthName . ' de ' .$carbonDate->locale('es_UY')->year;
        }

        $control = [
            'type' => 'green',
            'decision' => 'yes',
            'label' => $label, //etiqueta que aparecerá en el botón
            'api_action' => [
                'action' => 'wbsetclaim',
                'claim' => json_encode([
                    'id' => $qid . '$' . Str::uuid(),
                    'type' => 'statement',
                    'mainsnak' => [
                        'snaktype' => 'value',
                        'property' => 'P569',   // Date of birth
                        'datavalue' => [
                            'type' => 'time',
                            'value' => [
                                'after' => 0,
                                'before' => 0,
                                'calendarmodel' => 'http://www.wikidata.org/entity/Q1985727',   // Calendario Gregoriano
                                'precision' => $precission,
                                'time' => $carbonDate->format('\+Y-m-d\TH:i:s\Z'),
                                'timezone' => 0,
                            ],
                        ],
                    ],
                    'references' => [
                        $this->getBNUReference($bnuArtist),
                    ],
                    'rank' => 'normal',
                ]),
            ],
        ];

        return [$control];
    }

    private function getArtistsMissingDoB() : Collection
    {
        return $this->getCollectionFromCSV(
            path: resource_path('data/bnu-autores-sin-fecha-de-nacimiento(P569).csv'),
            keys: ['url', 'name', 'bnu_id'],
        )->skip(1);
    }
}
