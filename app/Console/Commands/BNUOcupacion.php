<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Tile;
use App\Traits\HandlesBNUArtists;
use App\Traits\HandlesCSV;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;

class BNUOcupacion extends Command
{
    use HandlesCSV, HandlesBNUArtists;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:bnu-ocupacion {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private Collection $occupations;
    private LazyCollection $bnuArtists;
    private Game $game;
    /** occupation */
    private string $property = 'P106';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->game = Game::updateOrCreate([
            'slug' => 'bnu-ocupacion',
        ], [
            'name' => 'Completa información del catálogo de la BNU',
            'description' => [
                'label' => [
                    'es' => 'Ayuda añadir información relacionada a Autores de la BNU',
                    'en' => 'Help adding missing information related to artists on BNU\'s catalog',
                ],
                'description' => [
                    'es' => 'Selecciona la opción que mejor se ajusta a la persona',
                    'en' => 'Select the option that fits best',
                ],
                'icon' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Biblioteca_Nacional_de_Uruguay_-_fachada.JPG/320px-Biblioteca_Nacional_de_Uruguay_-_fachada.JPG',
            ],
        ]);

        $offset = (int) $this->option('offset');

        $artistsMissingOccupation = $this->getArtistsMissingOccupation();
        $this->output->progressStart($artistsMissingOccupation->count());

        $this->output->progressAdvance($offset);

        $this->occupations = $this->getOccupations();
        $this->bnuArtists = $this->getArtistsFromBNU();

        $artistsMissingOccupation->skip($offset)->each($this->createTile(...));
        $this->output->progressFinish();


    }

    public function createTile(array $artist, int $index) : void
    {
        $this->output->progressAdvance();
        $bnu = $this->bnuArtists->firstWhere('id', $artist['bnu_id']);
        $occupations = $this->occupations->filter(function ($occupation) use ($bnu) {
            $search = [
                'nurse',
                'poetisa',
                'periodismo',
                'Ing. ',
                'expresión plástica',
                'Aviador',
                'Capitán de fragata',
                'Asesor financiero',
                'actriz',
            ];
            $replace = [
                'enfermero',
                'poeta',
                'periodista',
                'ingeniero',
                'artista plástico',
                'piloto de aviación',
                'oficial naval',
                'analista financiero',
                'actor',
            ];

            $bio = str_replace(
                search: $search,
                replace: $replace,
                subject: Arr::get($bnu, 'Datos Biog./Hist.', '')
            );
            $profession = $occupation['name'];
            if (Str::endsWith($profession, ['o', 'a'])) {
                $profession = mb_substr($profession, 0, -1);
            }

            return Str::contains($bio, $profession, true);
        });

        if ($occupations->isEmpty()) {
            file_put_contents(
                filename: resource_path('data/occupation-not-found.txt'),
                data: $artist['name'] . ': ' . Arr::get($bnu, 'Datos Biog./Hist.', '') . PHP_EOL,
                flags: FILE_APPEND
            );
            return;
        }

        $qid = str_replace('http://www.wikidata.org/entity/', '', $artist['url']);
        $tile = [
            'qid' => $qid,
            'property' => $this->property,
            'name' => $artist['name'],
            'game_id' => $this->game->id,
            'status' => Tile::PENDING,
            'sections' => [
                ['type' => 'item', 'q' => $qid],
                [
                    'type' => 'text',
                    'title' => $artist['name'] . ' - ¿principal ocupación (' . $this->property . ')?',
                    'text' => $bnu['Datos Biog./Hist.'],
                ],
            ],
            'controls' => $occupations->map(function (array $occupation) use ($qid) {
                $optionQid = str_replace('http://www.wikidata.org/entity/', '', $occupation['url']);
                return [
                    'type' => 'green',
                    'decision' => 'yes',
                    'label' => $occupation['name'] . ' (' . $optionQid . ')', //etiqueta que aparecerá en el botón
                    'api_action' => [
                        'action' => 'wbcreateclaim',
                        'entity' => $qid, // qid from the item to be modified
                        'property' => $this->property, // property to be modified
                        'snaktype' => 'value',
                        'value' => json_encode([
                            'entity-type' => 'item',
                            'id' => $optionQid,
                        ])
                    ],
                ];
            })->values()->toArray(),
        ];

        Tile::updateOrCreate(['qid' => $qid, 'property' => $this->property], $tile);
    }

    private function getArtistsMissingOccupation() : Collection
    {
        return $this->getCollectionFromCSV(
            path: resource_path('data/bnu-autores-sin-ocupacion.csv'),
            keys: ['url', 'name', 'bnu_id'],
        );
    }

    private function getOccupations() : Collection
    {
        return $this->getCollectionFromCSV(
            path: resource_path('data/ocupaciones-uruguay-wikidata(P106-de-uruguayos).csv'),
            keys: ['url', 'name']
        );
    }
}
