<?php

namespace App\Console\Commands;

use App\Models\Game;
use App\Models\Tile;
use App\Services\Wikidata;
use App\Traits\HandlesBNUArtists;
use App\Traits\HandlesCSV;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use RuntimeException;

class BNUPlaceOfBirth extends Command
{
    use HandlesCSV, HandlesBNUArtists;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:bnu-pob {--offset=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private Game $game;
    private LazyCollection $bnuArtists;
    private Wikidata $api;
    private array $uyProvinces = [
        "Cerro Largo" => 'Q16575',
        "Paysandú" => 'Q16576',
        "Canelones" => 'Q16577',
        "Flores" => 'Q16578',
        "San José" => 'Q16579',
        "Colonia" => 'Q16580',
        "Rocha" => 'Q16582',
        "Soriano" => 'Q16584',
        "Tacuarembó" => 'Q16587',
        "Durazno" => 'Q16591',
        "Florida" => 'Q16593',
        "Montevideo" => 'Q16594',
        "Salto" => 'Q16595',
        "Río Negro" => 'Q16596',
        "Artigas" => 'Q16603',
        "Rivera" => 'Q16609',
        "Treinta y Tres" => 'Q16610',
        "Maldonado" => 'Q331196',
        "Lavalleja" => 'Q460435',
    ];
    private Collection $uyCities;
    private array $options = [];
    /** Place of birth */
    private string $property = 'P19';

    /**
     * Execute the console command.
     */
    public function handle(Wikidata $api)
    {
        $this->api = $api;
        $this->game = Game::where('slug', 'bnu-ocupacion')->firstOrFail();

        $offset = (int) $this->option('offset');

        $artistsMissingPoB = $this->getArtistsMissingPoB();
        $this->output->progressStart($artistsMissingPoB->count());

        $this->output->progressAdvance($offset);

        $this->bnuArtists = $this->getArtistsFromBNU();
        $this->uyCities = $this->getCitiesFromUY();

        $artistsMissingPoB->skip($offset)->each($this->createTile(...));
        $this->output->progressFinish();
    }

    public function createTile(array $artist, int $index) : void
    {
        $qid = str_replace('http://www.wikidata.org/entity/', '', $artist['url']);
        // if (Tile::where('qid', $qid)->where('property', $this->property)->exists()) {
        //     if ($this->output->isVerbose()) {
        //         $this->line('');
        //     }
        //     return;
        // }
        $bnu = $this->bnuArtists->firstWhere('id', $artist['bnu_id']);
        $this->output->progressAdvance();

        try {
            $tile = [
                'qid' => $qid,
                'property' => $this->property,
                'name' => $artist['name'],
                'game_id' => $this->game->id,
                'status' => Tile::PENDING,
                'sections' => [
                    ['type' => 'item', 'q' => $qid],
                    [
                        'type' => 'text',
                        'title' => $artist['name'] . ' - ¿Lugar de nacimiento (' . $this->property . ')?',
                        'text' => $bnu['Datos Biog./Hist.'],
                    ],
                ],
                'controls' => $this->getControls($qid, $bnu),
            ];
            Tile::updateOrCreate(['qid' => $qid, 'property' => $this->property], $tile);
        } catch(RuntimeException) {
            file_put_contents(
                filename: resource_path('data/pob-not-found.txt'),
                data: $artist['name'] . ': ' . Arr::get($bnu, 'Datos Biog./Hist.', '') . PHP_EOL,
                flags: FILE_APPEND
            );
        }

    }

    private function getControls(string $qid, array $bnuArtist) : array
    {
        if (!preg_match('/n\.\s+en\s+(\w+)/i', $bnuArtist['Datos Biog./Hist.'], $matches)) {
            throw new RuntimeException('Place of birth not found');
        }
        $controls = [];

        $this->line($bnuArtist['Datos Biog./Hist.']);
        // Try to find cities
        foreach ($this->uyCities as $city) {
            if (str_contains($bnuArtist['Datos Biog./Hist.'], $city['name'])) {
                $optionQid = str_replace('http://www.wikidata.org/entity/', '', $city['url']);
                $label = "{$city['name']} (Ciudad de Uruguay) - {$optionQid}";
                $this->info($label);
                $controls[] = $this->createControlFromOption($qid, $optionQid, $label, $bnuArtist);
            }
        }

        if (count($controls) <= 1) {
            // Try to find provinces
            foreach (array_keys($this->uyProvinces) as $province) {
                if (str_contains($bnuArtist['Datos Biog./Hist.'], $province)) {
                    $optionQid = $this->uyProvinces[$province];
                    $label = "Departamento de {$province} ({$optionQid})";
                    $this->info($label);
                    $controls[] = $this->createControlFromOption($qid, $optionQid, $label, $bnuArtist);
                }
            }
        }

        if (empty($controls)) {
            $this->options[$matches[1]] = $this->api->search($matches[1]);
            foreach ($this->options[$matches[1]] as $option) {
                $label = "{$option['label']} (" . Arr::get($option, 'description', '') . " - {$option['id']})";
                $this->info($label);
                $controls[] = $this->createControlFromOption($qid, $option['id'], $label, $bnuArtist);
            }
        }

        return $controls;
    }

    private function createControlFromOption(string $qid, string $optionQid, string $label, array $bnuArtist) : array
    {
        return [
            'type' => 'green',
            'decision' => 'yes',
            'label' => $label, //etiqueta que aparecerá en el botón
            'api_action' => [
                'action' => 'wbsetclaim',
                'claim' => json_encode([
                    'id' => $qid . '$' . Str::uuid(),
                    'type' => 'statement',
                    'mainsnak' => [
                        'snaktype' => 'value',
                        'property' => $this->property,   // Place of birth
                        'datavalue' => [
                            'type' => 'wikibase-entityid',
                            'value' => [
                                'id' => $optionQid,
                            ],
                        ],
                    ],
                    'references' => [
                        $this->getBNUReference($bnuArtist),
                    ],
                    'rank' => 'normal',
                ]),
            ],
        ];
    }

    private function getArtistsMissingPoB() : Collection
    {
        return $this->getCollectionFromCSV(
            path: resource_path('data/bnu-autores-sin-lugar-de-nacimiento(P19).csv'),
            keys: ['url', 'name', 'bnu_id'],
        )->skip(1);
    }

    private function getCitiesFromUY() : Collection
    {
        return $this->getCollectionFromCSV(
            path: resource_path('data/ciudades-de-uruguay.csv'),
            keys: ['url', 'name'],
        );
    }
}
