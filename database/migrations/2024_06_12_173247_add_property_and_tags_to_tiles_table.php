<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tiles', function (Blueprint $table) {
            $table->string('property')->nullable();

            $table->dropUnique(['qid']);
            $table->unique(['qid', 'property']);

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tiles', function (Blueprint $table) {
            $table->dropUnique(['qid', 'property']);
            $table->unique(['qid']);

            $table->dropColumn('property');
        });
    }
};
