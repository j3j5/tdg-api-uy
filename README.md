# The Distributed Game APIs

## Nomenclator
A game to complete info from the streets of Montevideo. Find the entity that each street is named after.

### Datasets
Data to fill in Wikidata | `resources/data/streets-no-named-after.json` | https://w.wiki/A692

Street Info | `resources/data/nomenclator-mvd.csv` | https://catalogodatos.gub.uy/dataset/nomenclator-de-calles-de-montevideo-y-area-metropolitana

Street Classification | `resources/data/calles_marzo2023.csv` | https://catalogodatos.gub.uy/dataset/data-clasificacion-de-vias-de-montevideo-atunombre-uy

# TODO
- Leaderboard per game

# Queries
- Artistas y su ocupación --> https://w.wiki/ALML
- Ocupaciones de Uruguayos --> https://w.wiki/A74L
- Artistas SIN ocupación --> https://w.wiki/A73L
- Artistas SIN fecha de nacimiento --> https://w.wiki/ALuL
- Artistas SIN lugar de nacimiento --> https://w.wiki/ANRQ
- Ciudades de Uruguay --> https://w.wiki/ANRP